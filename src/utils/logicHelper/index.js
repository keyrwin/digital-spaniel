import { both, compose, complement, either, equals, isEmpty, isNil, not } from 'ramda'

export const notEmpty = compose(not, isEmpty)

export const notNull = compose(not, isNil)

export const notEquals = complement(equals)

export const neitherNullNorEmpty = both(notEmpty, notNull)

export const eitherNullOrEmpty = either(isEmpty, isNil)
