import { extendTheme } from '@chakra-ui/react'

const customTheme = extendTheme({
  semanticTokens: {
    colors: {
      error: 'red.500',
      success: 'green.500',
      primary: {
        default: '#506473'
      },
      secondary: {
        default: '#19293A'
      },
      tertiary: {
        default: '#C0345E'
      }
    }
  }
})

export default customTheme
