import React, { useCallback, useEffect, useState } from 'react'
import { Fade } from '@chakra-ui/react'
import PropTypes from 'prop-types'

import Header from 'components/Header'
import { LayoutContainer } from './styled'

const Layout = ({ children }) => {
  const [headerState, setHeaderState] = useState(true)

  const headerStateHandler = useCallback(() => {
    const top = window?.pageYOffset || document?.documentElement?.scrollTop
    if (top > 200) {
      setHeaderState(false)
    } else {
      setHeaderState(true)
    }
  }, [setHeaderState])

  useEffect(() => {
    window.addEventListener('scroll', headerStateHandler)
    return () => window.removeEventListener('scroll', headerStateHandler)
  }, [])

  return (
    <LayoutContainer>
      <Fade in={headerState}>
        <Header />
      </Fade>
      {children}
    </LayoutContainer>
  )
}

Layout.propTypes = {
  children: PropTypes.any
}

export default Layout
