import styled from '@emotion/styled'
import { Link } from '@chakra-ui/react'

export const StyledLink = styled(Link)`
  &:hover {
    text-decoration: none;
  }
  Button {
    padding: ${({ nopadding }) => (nopadding === 'true' ? 0 : undefined)};
  }
  Button:hover {
    background: transparent !important;
    text-decoration: none;
  }
  Button:hover::before {
    content: '';
    position: absolute;
    bottom: 0;
    left: ${({ hoverleft }) => hoverleft || '0%'};
    width: ${({ hoverwidth }) => hoverwidth || '100%'};
    border-bottom: ${({ hovercolor }) => `2px solid ${hovercolor || '#fff'}`};
  }
`
