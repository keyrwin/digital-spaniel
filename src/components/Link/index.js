import React from 'react'
import PropTypes from 'prop-types'

import { StyledLink } from './styled'

const Link = ({ to, nopadding, ...props }) => {
  return <StyledLink href={to} nopadding={String(nopadding)} {...props} />
}

Link.defaultProps = {
  to: '#',
  nopadding: false
}

Link.propTypes = {
  to: PropTypes.string,
  nopadding: PropTypes.any
}

export default Link
