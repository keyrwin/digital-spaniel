import React from 'react'
import PropTypes from 'prop-types'
import { Button, Flex, IconButton, Slide } from '@chakra-ui/react'
import { CloseIcon } from '@chakra-ui/icons'

import Link from 'components/Link'
import { MobileContainer } from './styled'

const HeaderMobile = ({ tabs, display, changeDisplay }) => {
  return (
    <MobileContainer display={display} bgColor='gray.50'>
      <Slide in={display === 'flex'} direction='top'>
        <Flex justify='flex-end'>
          <IconButton
            mt={10}
            mr='72px'
            aria-label='Open Menu'
            size='lg'
            color='gray.400'
            bgColor='gray.200'
            icon={<CloseIcon />}
            onClick={() => changeDisplay('none')}
          />
        </Flex>

        <Flex flexDir='column' align='center' color='#000'>
          {tabs.map(({ label, route }) => (
            <Link to={route} key={route} hoverleft='15%' hoverwidth='70%' hovercolor='#C0345E'>
              <Button variant='ghost' aria-label={label}>
                {label}
              </Button>
            </Link>
          ))}
        </Flex>
      </Slide>
    </MobileContainer>
  )
}

HeaderMobile.propTypes = {
  tabs: PropTypes.array.isRequired,
  display: PropTypes.string,
  changeDisplay: PropTypes.func.isRequired
}

export default HeaderMobile
