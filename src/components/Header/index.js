import React from 'react'
import { Image } from '@chakra-ui/react'

import HeaderDesktop from './Desktop'
import HeaderMobile from './Mobile'
import { HeaderLayout } from './styled'
import Logo from 'assets/images/digital-spaniel-logo.png'

import useHeaderStore from './store'

const TABS = [
  { label: 'Services', route: '#Services' },
  { label: 'Work', route: '#Work' },
  { label: 'About', route: '#About' },
  { label: 'Blog', route: '#Blog' },
  { label: 'Contact', route: '#Contact' }
]

const Header = () => {
  const { headerCollpased, setHeaderCollpased } = useHeaderStore()
  return (
    <HeaderLayout p={['32px', '32px 64px', '32px 64px', '32px 64px']}>
      <Image src={Logo} alt='Digitial Spaniel' sx={{ width: '150px', aspectRatio: '213 / 100' }} />
      <HeaderDesktop tabs={TABS} changeDisplay={setHeaderCollpased} />
      <HeaderMobile tabs={TABS} changeDisplay={setHeaderCollpased} display={headerCollpased} />
    </HeaderLayout>
  )
}

export default Header
