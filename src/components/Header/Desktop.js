import React from 'react'
import PropTypes from 'prop-types'
import { Button, Flex, IconButton } from '@chakra-ui/react'
import { HamburgerIcon } from '@chakra-ui/icons'

import Link from 'components/Link'

const HeaderDesktop = ({ tabs, changeDisplay }) => {
  return (
    <Flex>
      <Flex display={['none', 'none', 'flex', 'flex']}>
        {tabs.map(({ label, route }) => (
          <Link to={route} key={route} hoverleft='15%' hoverwidth='70%'>
            <Button variant='ghost' aria-label={label}>
              {label}
            </Button>
          </Link>
        ))}
      </Flex>

      {/* Mobile */}
      <IconButton
        aria-label='Open Menu'
        size='lg'
        mr={2}
        bgColor='transparent'
        icon={<HamburgerIcon color='#000' boxSize='24px' />}
        onClick={() => changeDisplay('flex')}
        display={['flex', 'flex', 'none', 'none']}
      />
    </Flex>
  )
}

HeaderDesktop.propTypes = {
  tabs: PropTypes.array.isRequired,
  changeDisplay: PropTypes.func.isRequired
}

export default HeaderDesktop
