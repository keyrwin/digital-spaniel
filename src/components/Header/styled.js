import styled from '@emotion/styled'
import { Flex } from '@chakra-ui/react'

export const HeaderLayout = styled(Flex)`
  width: 100%;
  align: center;
  align-items: center;
  justify-content: space-between;
  box-size: full;
  color: #FFFFFF;

  transition: top 0.2s ease-in-out;
  overflow-y: hidden;
  borderBottomWidth={2}
  position: fixed;
  top: 0;
`

export const MobileContainer = styled(Flex)`
  width: 100vw;
  height: 100vh;
  z-index: 20;
  position: fixed;
  top: 0;
  left: 0;
  overflow-y: auto;
  flex-direction: column;
`
