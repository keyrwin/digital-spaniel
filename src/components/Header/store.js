import create from 'zustand'

const useHeaderStore = create((set) => ({
  headerCollpased: 'none',
  setHeaderCollpased: (value) => {
    set(() => ({ headerCollpased: value }))
  }
}))

export default useHeaderStore
