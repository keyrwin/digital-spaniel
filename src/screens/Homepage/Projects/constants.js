import image1 from 'assets/images/projects/whellies01.png'
import image2 from 'assets/images/projects/newspaper02.png'
import image3 from 'assets/images/projects/makerek.png'
import image4 from 'assets/images/projects/newspaper.png'
import image5 from 'assets/images/projects/rider01.png'

export const AVAILABLE_TABS = [
  { label: 'All', images: [image1, image2, image3, image4, image5] },
  { label: 'Branding', images: [image1, image2, image3, image4, image5] },
  { label: 'Web Design', images: [image1, image2, image3, image4, image5] },
  { label: 'Digital Marketing', images: [image1, image2, image3, image4, image5] }
]
