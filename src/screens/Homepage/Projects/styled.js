import styled from '@emotion/styled'
import { Flex } from '@chakra-ui/react'

export const ProjectContainer = styled(Flex)`
  min-width: 100%;
  min-height: 100vh;
  flex-direction: column;
  padding: 5% 12%;
`

export const TextOverlay = styled(Flex)`
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-end;
  padding: 0 32px;
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  height: 100%;
  width: 100%;
  opacity: 0;
  transition: 0.5s ease;
  background-color: #00000077;

  p {
    color: white;
  }
`
