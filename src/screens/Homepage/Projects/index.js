import React, { useState } from 'react'
import { addIndex, map } from 'ramda'
import { Box, Image, Text, Tabs, Tab, TabList, TabPanels, TabPanel } from '@chakra-ui/react'

import { AVAILABLE_TABS } from './constants'
import { ProjectContainer, TextOverlay } from './styled'

const mapIndexed = addIndex(map)
const selectedTabStyle = { fontWeight: 500, borderColor: 'tertiary' }

const Projects = () => {
  const [overlay, setOverlay] = useState(-1)
  return (
    <ProjectContainer mt={['650px', '650px', 0, 0]}>
      <Box fontSize={38} fontWeight='500' mb={8}>
        <Text color='secondary'>Some of our</Text>
        <Text color='primary' mt='-14px'>
          recent projects
        </Text>
      </Box>
      <Box>
        <Tabs>
          <TabList mb={8}>
            {map(
              ({ label }) => (
                <Tab key={label} _selected={selectedTabStyle}>
                  {label}
                </Tab>
              ),
              AVAILABLE_TABS
            )}
          </TabList>
          <TabPanels>
            {map(
              ({ label, images }) => (
                <TabPanel
                  key={label}
                  _selected={selectedTabStyle}
                  display='flex'
                  flexWrap='wrap'
                  columnGap={8}
                  rowGap={6}>
                  {mapIndexed((image, idx) => {
                    return (
                      <Box
                        key={idx}
                        position='relative'
                        onMouseEnter={() => setOverlay(idx)}
                        onMouseLeave={() => setOverlay(-1)}
                        _hover={{
                          cursor: 'pointer'
                        }}>
                        <Image
                          src={image}
                          alt={label}
                          fit='cover'
                          sx={{ minH: '200px', minW: '200px' }}
                        />
                        <TextOverlay opacity={`${overlay === idx ? 1 : 0} !important`}>
                          <Text fontSize={18} mb={2}>
                            Make Ideas Happen
                          </Text>
                          <Text mb={4}>
                            A local paper with big ideas needed A sharp new brand to inspire
                            readers.
                          </Text>
                          <Text fontSize={18} mb={14} _hover={{ borderBottom: '2px solid #fff' }}>
                            Full project
                          </Text>
                        </TextOverlay>
                      </Box>
                    )
                  }, images)}
                </TabPanel>
              ),
              AVAILABLE_TABS
            )}
          </TabPanels>
        </Tabs>
      </Box>
    </ProjectContainer>
  )
}

export default Projects
