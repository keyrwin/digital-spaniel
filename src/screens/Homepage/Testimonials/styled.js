import styled from '@emotion/styled'
import { Button, Flex } from '@chakra-ui/react'

export const Container = styled(Flex)`
  min-width: 100%;
  min-height: 100vh;
  flex-direction: column;
  padding: 5% 12%;
`

export const StyledDot = styled(Button)`
  border-radius: 50%;
  background-color: ${({ currentstate }) => (currentstate === 'active' ? '#19293a' : '#c5c9cd')};
  margin: 0 12px;
`

export const TestimonialWrapper = styled(Flex)`
  padding: 42px;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  background: ${({ isactive }) => (isactive === 'true' ? '#19293a' : '#e2e4e6')};
  border-radius: 10px;
  min-width: 300px;
  max-width: 400px;
  aspect-ratio: 0.6546003016591252;
`
