import React, { useCallback, useEffect, useState } from 'react'
import { always, ifElse, includes, map } from 'ramda'
import PropTypes from 'prop-types'
import { Box, Image, Text } from '@chakra-ui/react'
import Carousel from 'react-multi-carousel'
import 'react-multi-carousel/lib/styles.css'

import { neitherNullNorEmpty } from 'utils/logicHelper'
import useHomepageStore from '../store'
import { CAROUSEL_OPTIONS } from './constants'
import { Container, TestimonialWrapper, StyledDot } from './styled'

const CustomDot = ({ onClick, active }) => {
  return (
    <StyledDot
      currentstate={active ? 'active' : 'inactive'}
      onClick={onClick}
      size={active ? 'sm' : 'xs'}
    />
  )
}

CustomDot.propTypes = {
  onClick: PropTypes.func,
  active: PropTypes.any
}

const Testimonials = () => {
  const [currentSlide, setCurrentSlide] = useState(0)
  const { testimonials, getTestimonials } = useHomepageStore()

  const afterChangeHandler = useCallback((_, { currentSlide }) => {
    setCurrentSlide(currentSlide)
  }, [])

  useEffect(() => {
    getTestimonials()
  }, [])

  return (
    <Container>
      <Box fontSize={38} fontWeight='500' mb={8}>
        <Text color='secondary'>Kind words</Text>
        <Text color='primary' mt='-14px'>
          from our clients
        </Text>
      </Box>
      <Box pb={14} position='relative'>
        {ifElse(
          neitherNullNorEmpty,
          (array) => (
            <Carousel
              customDot={<CustomDot testimonials={testimonials} />}
              afterChange={afterChangeHandler}
              {...CAROUSEL_OPTIONS}>
              {map(({ id, image, message, name, title }) => {
                const isActive = includes(currentSlide, [id - 1, id + 2, id + 5])
                return (
                  <TestimonialWrapper key={id} isactive={String(isActive)}>
                    <Text
                      color={isActive ? '#ffffff' : 'primary'}
                      fontSize={[12, 16, 20, 24]}
                      fontStyle='italic'>
                      {message}
                    </Text>
                    <Image src={image} alt={name} m={8} mb={2} />
                    <Text
                      color={isActive ? '#ffffff' : 'secondary'}
                      fontSize={[12, 16, 24, 24]}
                      fontWeight={500}>
                      {name}
                    </Text>
                    <Text
                      color={isActive ? '#ffffff' : 'secondary'}
                      fontSize={[12, 16, 16, 18]}
                      fontWeight={500}>
                      {title}
                    </Text>
                  </TestimonialWrapper>
                )
              }, array)}
            </Carousel>
          ),
          always(null)
        )(testimonials)}
      </Box>
    </Container>
  )
}

export default Testimonials
