export const CAROUSEL_OPTIONS = {
  additionalTransfrom: 0,
  arrows: true,
  autoPlaySpeed: 3000,
  centerMode: false,
  className: '',
  containerClass: 'container',
  dotListClass: '',
  focusOnSelect: true,
  infinite: true,
  itemClass: '',
  partialVisible: true,
  pauseOnHover: true,
  renderArrowsWhenDisabled: false,
  renderDotsOutside: true,
  responsive: {
    desktop: {
      breakpoint: {
        max: 3000,
        min: 1500
      },
      items: 2,
      partialVisibilityGutter: 200
    },
    tablet: {
      breakpoint: {
        max: 1500,
        min: 1024
      },
      items: 1
    },
    mobile: {
      breakpoint: {
        max: 464,
        min: 0
      },
      items: 1,
      partialVisibilityGutter: 0
    }
  },
  rewind: true,
  rewindWithAnimation: true,
  rtl: false,
  shouldResetAutoplay: true,
  showDots: true,
  sliderClass: '',
  slidesToSlide: 1,
  swipeable: false
}
