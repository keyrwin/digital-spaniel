export default [
  {
    label: 'Explore',
    value: 'explore',
    children: [
      { label: 'Services', value: 'services' },
      { label: 'Work', value: 'work' },
      { label: 'About', value: 'about' },
      { label: 'Blog', value: 'blog' },
      { label: 'Careers', value: 'careers' }
    ]
  },
  {
    label: 'Services',
    value: 'services',
    children: [
      { label: 'Brand', value: 'brand' },
      { label: 'Development', value: 'development' },
      { label: 'Marketing', value: 'marketing' }
    ]
  },
  {
    label: 'Questions',
    value: 'questions',
    children: [
      { label: 'Call Us', value: 'call' },
      { label: '0121 345 678', value: 'call_value' },
      { label: 'Email Us', value: 'email' },
      { label: 'info@digitalspaniel.co.uk', value: 'email_value' }
    ]
  }
]
