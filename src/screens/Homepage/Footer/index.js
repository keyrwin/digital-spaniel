import React, { useState } from 'react'
import { always, equals, ifElse, includes, map } from 'ramda'
import { Box, Button, Flex, Grid, GridItem, Text } from '@chakra-ui/react'
import { FaFacebookF, FaTwitter, FaInstagram, FaLinkedinIn } from 'react-icons/fa'

import Link from 'components/Link'
import {
  CopyrightText,
  FooterContainer,
  FooterTextContainer,
  FooterOptionsContainer,
  StyledIcon,
  StyledLink
} from './styled'

import constants from './constants'

const Footer = () => {
  const [hovered, setHovered] = useState('')
  return (
    <FooterContainer display={['block', 'block', 'flex']} position='relative'>
      <FooterTextContainer mt={[12, 12, 0, 0]} pt={[12, 12, 0, 0]} mb={[12, 12, 0, 0]}>
        <Box maxW={['80%', '80%', '50%']}>
          <Box fontSize={38} fontWeight='500' mb={8}>
            <Text color='secondary'>We&apos;re a brands</Text>
            <Text color='primary' mt='-14px'>
              best friend
            </Text>
          </Box>
          <Box mb={8}>
            <Text color='primary'>
              By focusing on design as an enabler and putting a huge emphasis on our clients as
              co-producers, we create innovative, sustainable marketing that enhances brand
              experience and user engagement.
            </Text>
          </Box>
          <Box mb={12}>
            <Link to='#ourprocess' hovercolor='#C0345E' nopadding>
              <Button variant='ghost' fontSize={20}>
                Let&apos;s talk
              </Button>
            </Link>
          </Box>
        </Box>
      </FooterTextContainer>
      <FooterOptionsContainer pb={['70%', '35%', '5%', 0, 0]} pl={[0, 0, '10%', '10%']}>
        <Grid
          w='100%'
          textAlign={['center', 'center', 'left']}
          templateColumns={['repeat(1, 1fr)', 'repeat(1, 1fr)', 'repeat(2, 1fr)', 'repeat(3, 1fr)']}
          gap={6}>
          {map(({ label, value, children }) => {
            return (
              <GridItem key={value}>
                <Button variant='ghost' fontSize={20} color='secondary' p={0}>
                  {label}
                </Button>
                <Flex flexDir='column'>
                  {map(({ label: childrenLabel, value: childValue }) => {
                    return (
                      <StyledLink
                        key={childValue}
                        to={childrenLabel}
                        color='primary'
                        onMouseEnter={() => setHovered(childValue)}
                        onMouseLeave={() => setHovered('')}>
                        <Button
                          variant='ghost'
                          fontSize={includes(childValue, ['call_value', 'email_value']) ? 16 : 20}
                          fontWeight={
                            includes(childValue, ['call_value', 'email_value']) ? 'none' : '500'
                          }>
                          {childrenLabel}
                          {ifElse(
                            (hoveredVal) =>
                              equals(childValue, hoveredVal) &&
                              !includes(childValue, ['call_value', 'email_value']),
                            always(<StyledIcon />),
                            always(null)
                          )(hovered)}
                        </Button>
                      </StyledLink>
                    )
                  }, children)}
                </Flex>
              </GridItem>
            )
          }, constants)}
        </Grid>
      </FooterOptionsContainer>
      <CopyrightText>
        <Text color='#50647380' fontWeight={500}>
          Copyright © Digital Spaniel 2019. All rights reserved.
        </Text>
        <Flex>
          <FaFacebookF color='#9EA9B1' size={32} style={{ marginRight: '14px' }} />
          <FaTwitter color='#9EA9B1' size={32} style={{ marginRight: '14px' }} />
          <FaInstagram color='#9EA9B1' size={32} style={{ marginRight: '14px' }} />
          <FaLinkedinIn color='#9EA9B1' size={32} style={{ marginRight: '14px' }} />
        </Flex>
      </CopyrightText>
    </FooterContainer>
  )
}

export default Footer
