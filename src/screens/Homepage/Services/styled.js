import styled from '@emotion/styled'
import { Flex, Link } from '@chakra-ui/react'
import { ChevronRightIcon } from '@chakra-ui/icons'

export const ServicesContainer = styled(Flex)`
  width: 100%;
  height: 70vh;
  background: #edeff1;
  align-items: center;
`

export const ServicesTextContainer = styled(Flex)`
  flex: 1;
  height: 100%;
  align-items: center;
  justify-content: center;
`

export const ServicesOptionsContainer = styled(Flex)`
  flex: 1;
  height: 100%;
  align-items: center;
`

export const StyledLink = styled(Link)`
  &:hover {
    text-decoration: none;
  }
  Button {
    padding: 0;
  }
  Button:hover {
    background: transparent !important;
    text-decoration: none;
  }
`

export const StyledIcon = styled(ChevronRightIcon)`
  background: #506473;
  border-radius: 50%;
  color: #fff;
  margin-top: 2px;
  margin-left: 8px;
`
