export default [
  {
    label: 'Brand',
    value: 'brand',
    children: [
      { label: 'Brand Strategy', value: 'brand_strategy' },
      { label: 'Logo & Name', value: 'logo_and_name' },
      { label: 'Identity & Collateral', value: 'identity_and_collateral' }
    ]
  },
  {
    label: 'Development',
    value: 'development',
    children: [
      { label: 'eCommerce', value: 'ecommerce' },
      { label: 'Web Development', value: 'web_development' },
      { label: 'Mobile Apps', value: 'mobile_apps' }
    ]
  },
  {
    label: 'Marketing',
    value: 'marketing',
    children: [
      { label: 'Digital', value: 'digital' },
      { label: 'Market Research', value: 'market_research' }
    ]
  }
]
