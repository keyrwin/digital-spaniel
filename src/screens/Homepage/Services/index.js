import React, { useState } from 'react'
import { always, equals, ifElse, map } from 'ramda'
import { Box, Button, Grid, GridItem, Flex, Text } from '@chakra-ui/react'

import Link from 'components/Link'
import {
  ServicesContainer,
  ServicesTextContainer,
  ServicesOptionsContainer,
  StyledIcon,
  StyledLink
} from './styled'

import constants from './constants'

const Services = () => {
  const [hovered, setHovered] = useState('')
  return (
    <ServicesContainer display={['block', 'block', 'flex']}>
      <ServicesTextContainer>
        <Box maxW={['80%', '80%', '50%']}>
          <Box fontSize={38} fontWeight='500' mb={8}>
            <Text color='secondary'>What are</Text>
            <Text color='primary' mt='-14px'>
              we capable of
            </Text>
          </Box>
          <Box mb={8}>
            <Text color='primary'>
              By focusing on design as an enabler and putting a huge emphasis on our clients as
              co-producers, we create innovative, sustainable marketing that enhances brand
              experience and user engagement.
            </Text>
          </Box>
          <Box>
            <Link to='#ourprocess' hovercolor='#C0345E' nopadding>
              <Button variant='ghost' fontSize={20}>
                Our process
              </Button>
            </Link>
          </Box>
        </Box>
      </ServicesTextContainer>
      <ServicesOptionsContainer mt={[14, 12, 0, 0]}>
        <Grid
          w='100%'
          textAlign={['center', 'center', 'left']}
          templateColumns={['repeat(1, 1fr)', 'repeat(1, 1fr)', 'repeat(2, 1fr)', 'repeat(3, 1fr)']}
          gap={6}>
          {map(({ label, value, children }) => {
            return (
              <GridItem key={value}>
                <Button variant='ghost' fontSize={20} color='secondary' p={0}>
                  {label}
                </Button>
                <Flex flexDir='column'>
                  {map(({ label: childrenLabel, value: childValue }) => {
                    return (
                      <StyledLink
                        key={childValue}
                        to={childrenLabel}
                        color='primary'
                        onMouseEnter={() => setHovered(childValue)}
                        onMouseLeave={() => setHovered('')}>
                        <Button variant='ghost' fontSize={20}>
                          {childrenLabel}
                          {ifElse(
                            equals(childValue),
                            always(<StyledIcon />),
                            always(null)
                          )(hovered)}
                        </Button>
                      </StyledLink>
                    )
                  }, children)}
                </Flex>
              </GridItem>
            )
          }, constants)}
        </Grid>
      </ServicesOptionsContainer>
    </ServicesContainer>
  )
}

export default Services
