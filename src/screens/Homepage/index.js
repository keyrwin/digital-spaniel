import React from 'react'

import Main from './Main'
import Services from './Services'
import Projects from './Projects'
import Testimonials from './Testimonials'
import Footer from './Footer'
import Layout from 'components/Layout'

const Homepage = () => {
  return (
    <Layout>
      <Main />
      <Services />
      <Projects />
      <Testimonials />
      <Footer />
    </Layout>
  )
}

export default Homepage
