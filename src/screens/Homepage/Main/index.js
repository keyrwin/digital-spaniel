import React from 'react'
import { always, equals, ifElse } from 'ramda'
import { Box, Button, Image, Text } from '@chakra-ui/react'
import LeftImage from 'assets/images/spaniel_gradient.jpg'

import useHeaderStore from 'components/Header/store'
import Link from 'components/Link'
import { MainContainer, MainImage, MainText } from './styled'

const Main = () => {
  const { headerCollpased } = useHeaderStore()
  return (
    <MainContainer>
      <MainText>
        <Box maxW={['80%', '80%', '50%']}>
          <Box mb={8}>
            <Text color='#C0345ECC' fontWeight='medium'>
              BRAND, DEV, ECOM, MARKETING
            </Text>
          </Box>
          <Box fontSize={38} fontWeight='500' mb={8}>
            <Text color='secondary'>We unleash</Text>
            <Text color='primary' mt='-14px'>
              business potential
            </Text>
          </Box>
          <Box mb={8}>
            <Text color='primary'>
              We create brand experiences which are memorable and distinct. Our experienced team
              create and develop brands with personality and resonance.
            </Text>
          </Box>
          {ifElse(
            equals('none'),
            always(
              <Box>
                <Link to='#letstalk' hovercolor='#C0345E' nopadding>
                  <Button variant='ghost' fontSize={20}>
                    Let&apos;s talk
                  </Button>
                </Link>
              </Box>
            ),
            always(null)
          )(headerCollpased)}
        </Box>
      </MainText>
      <MainImage display={['none', 'none', 'block']}>
        <Image
          src={LeftImage}
          alt='Spaniel Gradient'
          fit='cover'
          sx={{ width: '100%', height: '100%' }}
        />
      </MainImage>
    </MainContainer>
  )
}

export default Main
