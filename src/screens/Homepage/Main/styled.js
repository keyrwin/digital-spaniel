import styled from '@emotion/styled'
import { Flex } from '@chakra-ui/react'

export const MainContainer = styled(Flex)`
  width: 100vw;
  height: 100vh;
  position: static;
`

export const MainImage = styled(Flex)`
  flex: 1;
  height: 100%;
  background: blue;
`

export const MainText = styled(Flex)`
  flex: 1;
  height: 100%;
  align-items: center;
  justify-content: center;
`
