import create from 'zustand'

const useHomepageStore = create((set) => ({
  testimonials: null,
  testimonialsLoading: false,
  testimonialsSuccess: false,
  testimonialsError: false,
  getTestimonials: async () => {
    try {
      set(() => ({
        testimonialsLoading: true,
        testimonialsSuccess: false,
        testimonialsError: false
      }))

      const response = await fetch('testimonials.json', {
        headers: {
          'Content-Type': 'application/json',
          Accept: 'application/json'
        }
      })
      const data = await response.json()

      set(() => ({
        testimonials: data,
        testimonialsSuccess: true
      }))
    } catch (err) {
      set(() => ({ testimonialsError: err }))
    } finally {
      set(() => ({ testimonialsLoading: false }))
    }
  }
}))

export default useHomepageStore
