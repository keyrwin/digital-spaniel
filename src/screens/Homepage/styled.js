import styled from '@emotion/styled'
import { Box } from '@chakra-ui/react'

export const Container = styled(Box)`
  width: 100%;
  height: 100%;
`
